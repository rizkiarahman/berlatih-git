<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>String PHP</title>
</head>

<body>
  <h1>Berlatih String PHP</h1>
  <h3> Soal No 1</h3>


  <?php

  $first_sentence = "Hello PHP!";
  echo "<p> Kalimat 1 : " . $first_sentence . "</p>";
  echo "<p> Panjang string : " . strlen($first_sentence) . "</p>";
  echo "<p> Jumlah kata : " . str_word_count($first_sentence) . "</p>";

  $second_sentence = "I'm ready for the challenges";
  echo "<p> Kalimat 2 : " . $second_sentence . "</p>";
  echo "<p> Panjang string : " . strlen($second_sentence) . "</p>";
  echo "<p> Jumlah kata : " . str_word_count($second_sentence) . "</p>";


  echo "<h3> Soal No 2 </h3>";


  $string2 = "I love PHP";

  echo "<label> String: </label>  $string2 <br>";
  echo "Kata pertama: " . substr($string2, 0, 1) . "<br>";
  // Lanjutkan di bawah ini
  echo "Kata kedua: " . substr($string2, 2, 5) . "<br>";
  echo "Kata ketiga: " . substr($string2, 7, 9) . "<br>";

  echo "<h3> Soal No 3 </h3>";

  /*
  SOAL NO 3
  Mengubah karakter atau kata yang ada di dalam sebuah string.
  */
  $string3 = "PHP is old but sexy!";
  echo "String: $string3 <br>";
  // OUTPUT : "PHP is old but awesome"
  echo "Ganti Kata String : " . str_replace("sexy!", "awesome", $string3);

  ?>
</body>

</html>