<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <h2>Contoh Array</h2>

  <?php
  echo "<h3> Soal No 1 </h3>";

  $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
  print_r($kids);

  echo "<br>";

  $adult = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
  print_r($adult);

  echo "<h3> Soal No 2 </h3>";
  echo "Cast Stranger Things: ";
  echo "<br>";
  echo "<h4>Total Kids : " . count($kids) . "</h4>";
  echo "<ol>";
  echo "<li> $kids[0] </li>";
  echo "<li> $kids[1] </li>";
  echo "<li> $kids[2] </li>";
  echo "<li> $kids[3] </li>";
  echo "<li> $kids[4] </li>";
  echo "<li> $kids[5] </li>";
  echo "</ol>";
  echo "<h4>Total Adults  : " . count($adult) . "</h4>";
  echo "<ol>";
  echo "<li> $adult[0] </li>";
  echo "<li> $adult[1] </li>";
  echo "<li> $adult[2] </li>";
  echo "<li> $adult[3] </li>";
  echo "<li> $adult[4] </li>";
  echo "</ol>";

  echo "<h3> Soal No 3 </h3>";

  $cast = [
    ["Name" => "Will Byers", "Age" => 12, "Aliases" => "Will the Wise", "Status" => "Alive"],
    ["Name" => "Mike Wheeler", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"],
    ["Name" => "Jim Hopper", "Age" => 42, "Aliases" => "Chief Hopper", "Status" => "Deceased"],
    ["Name" => "Eleven", "Age" => 12, "Aliases" => "El", "Status" => "Alive"],
  ];

  echo "<pre>";
  print_r($cast);
  echo "</pre>";



  ?>
</body>

</html>