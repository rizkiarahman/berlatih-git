<?php

require_once("Animal.php");

class Frog extends Animal
{
  public $name = "buduk";
  public $legs = "4";
  public $cold_blooded = "no";

  public function Jump()
  {
    echo "Jump : Hop Hop <br> <br>";
  }
}
